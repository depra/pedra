#!/usr/bin/env python

from importlib_metadata import entry_points
from setuptools import setup

# PACKAGE METADATA
# ##################
NAME = 'pedra'
FULLNAME = "PEDRA"
VERSION = '0.1'
DESCRIPTION = 'PipEline for Data Reduction of Asteroids'
with open("README.rst") as f:
    LONG_DESCRIPTION = ''.join(f.readlines())
AUTHOR = '''M. De Pra'''
AUTHOR_EMAIL = 'mariondepra@gmail.com'
MAINTAINER = 'M. De Pra'
MAINTAINER_EMAIL = AUTHOR_EMAIL
URL = 'https://github.com/depra/pedra'
LICENSE = 'MIT License'

# TO BE INSTALLED
# ##################
PACKAGES = ['pedra', 'pedra.gui']

#PACKAGE_DATA = {'catuaba.data':['jwst/*']}

# SCRIPTS = ['scripts/catuaba']

# DEPENDENCIES
# ##################
INSTALL_REQUIRES = [
    'cana-asteroids',
    'pandas',
    'scipy',
    'numpy',
    'matplotlib',
    'pyyaml',
    'astropy',
    'photutils',
    'twirl',
    'imutils',
    'plotly',
    'opencv-python',
    'dynesty'
]

#DEPENDENCY_LINKS = ['git+https://github.com/PolyChord/PolyChordLite@master']

PYTHON_REQUIRES = ">=3.6"

if __name__ == '__main__':
    setup(name=NAME,
          description=DESCRIPTION,
          #   long_description=LONG_DESCRIPTION,
          version=VERSION,
          author=AUTHOR,
          author_email=AUTHOR_EMAIL,
          maintainer=MAINTAINER,
          maintainer_email=MAINTAINER_EMAIL,
          license=LICENSE,
          url=URL,
          #   platforms=PLATFORMS,
        #   scripts=SCRIPTS,
          packages=PACKAGES,
#          dependency_links = DEPENDENCY_LINKS,
#          entry_points={
#                'console_scripts' : [
#                    'catuaba = catuaba.script:main',
#                    ]
                    # },
          #   ext_modules=EXT_MODULES,
#           package_data=PACKAGE_DATA,
          #   classifiers=CLASSIFIERS,
          #   keywords=KEYWORDS,
          #   cmdclass=CMDCLASS,
          install_requires=INSTALL_REQUIRES,
          python_requires=PYTHON_REQUIRES,
          )
