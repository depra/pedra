
import numpy as np
import matplotlib.pyplot as plt
import itertools


def plot_sources(img, sources, img_kwargs={}, **kwargs):
    r"""
    """


def view_groups(imgdict, nrows, ncols, labels=None, view_args={}, **kwargs):
    r"""
    """
    fig, ax = plt.subplots(nrows, ncols, **kwargs)
    ax = np.ravel(ax)
    if isinstance(imgdict, dict):
        imgdict = list(imgdict.values())
        if isinstance(imgdict[0], list):
            imgdict = list(itertools.chain.from_iterable(imgdict))
    # print(len(imgdict), ax.shape)
    for i, im in enumerate(imgdict):
        # print(i, im)
        im.view(ax=ax[i], **view_args)
        # 
    return fig, ax