r''' Routines to perform calibrations on fits images
'''

import os
import numpy as np
import matplotlib.pyplot as plt
from copy import copy
from .core import Image


def biascorrect(img, bias, prefix='b'):
    r''' Bias correction for an image

    Parameters
    -----------
    fitsfiles: str
        Fits file of the image to be corrected

    biasfile: str
        Master bias fits file

    prefix: str
        Prefix of the output corrected image, if save=True. The corrected image will
        be placed on the same folder of the original fitsfile, with the prefix on the
        basename.

    Returns
    --------
    Corrected Image object
    '''
    if isinstance(bias, str):
        # loading bias
        bias = Image(bias)
    if isinstance(img, str):
        # removing bias
        img = Image(img)
    bimg = img - bias
    bimg.label = prefix + bimg.label
    bimg.hdr['PEDRA_BIASCORR'] = True
    return bimg


def flatcorrect(img, flat, prefix='f'):
    r''' Flat field correction for an image

    Parameters
    -----------
    fitsfiles: str
        Fits file of the image to be corrected

    flatfile: str
        Master flat fits file

    save: boolean
        True if want to save image.

    prefix: str
        Prefix of the output corrected image, if save=True. The corrected image will
        be placed on the same folder of the original fitsfile, with the prefix on the
        basename.

    plot: boolean
        If want to display corrected image

    ** kwargs: matplotlib kwargs for imshow

    Returns
    --------
    Corrected Image object
    '''
    fimg = img.divide(flat)
    fimg.label = prefix + img.label
    fimg.hdr['PEDRA_FLATCORR'] = True
    return fimg
