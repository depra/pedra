
import os
import datetime
import numpy as np
import matplotlib.pyplot as plt
from astropy.io import fits
from astropy import wcs
from copy import copy
import rebin

from cana.util import kwargupdate

from astropy.io.fits.verify import VerifyWarning
import warnings
warnings.simplefilter('ignore', category=VerifyWarning)

def loadimage_batch(imglist, data_ext=0, header_ext=0, wcs_ext=None,
                    err_ext=None, mask=None, labels=None, **kwargs):
    r"""
    """
    if labels is not None:
        imgs = [loadimage(img, labels[i], data_ext=data_ext, header_ext=header_ext,
                          wcs_ext=wcs_ext, err_ext=err_ext, mask=mask, **kwargs) for i, img in enumerate(imglist)]
    else:
        imgs = [loadimage(img, data_ext=data_ext, header_ext=header_ext,
                          wcs_ext=wcs_ext, err_ext=err_ext, mask=mask, **kwargs) for img in imglist]
    return imgs


def loadimage(imgfile, data_ext=0, header_ext=0, wcs_ext=None,
              err_ext=None, mask=None, label=None, **kwargs): # -> implement method to merge images or headers
    r"""
    Load Image from file. 

    Parameters
    ----------
    imgfile: string
        Image file path.

    data_ext: int 
        Fits file extention for image data. 
        Default is 0. 

    header_ext: int 
        Fits file extention for header info. 
        Default is 0. 
         
    wcs_ext: None or int (Optional)
        Fits file extention for WCS info. 
        Default is None, which will not get WCS.
        
    err_ext: None or int (Optional)
        Fits file extention for WCS info. 
        Default is None, which will not get an error array.
        
    mask: None or np.ndarray (Optional)
        Boolean numpy array for masking pixels in the image.
        Shape must be equal to data. Default is None.

    label: string (Optional)
        Image label.
    
    Returns
    -------
    PEDRA Image Object.    
    """
    if label is None:
        label = os.path.basename(imgfile)
    hdu = fits.open(imgfile, **kwargs)   
    hdr = hdu[header_ext].header 
    arr = np.array(hdu[data_ext].data, dtype=np.float64, order='F')
    if wcs_ext is not None:
        wcs_ext = fits.getheader(imgfile, ext=wcs_ext)
        wcs_ext = wcs.WCS(wcs_ext)
    if err_ext is not None:
        err_ext = np.array(hdu[err_ext].data, dtype=np.float64, order='F')
    img = Image(arr, hdr, wcs_ext, err_ext, mask, label)
    return img


class Image(object):
    r'''
    Class to manipulate a fits Image file.
    '''

    def __init__(self, data, hdr, wcs=None, err=None, mask=None, label='image'):
        r'''
        Initialize Image object.

        Parameters
        ----------
        data: np.ndarray
            The numpy array containing the data for the image.
        
        hdr: fits.header.Header
            Astropy FITS header object.
        
        wcs: wcs.WCS (Optional)
            Astropy WCS object with matrix for world coordinate system.
        
        err: None or np.ndarray (Optional)
            The numpy array containing the error associated with the image data.
            Shape must be equal to data.

        mask: None or np.ndarray (Optional)
            Boolean numpy array for masking pixels in the image.
            Shape must be equal to data.

        label: string (Optional)
            Image label.
        '''
        if err is not None:
            assert err.shape == data.shape
        if mask is not None:
            assert err.shape == data.shape
        self.data = data
        self.hdr = hdr  
        self.label = label
        self.wcs = wcs
        self.err = err
        self.mask = mask

    @property
    def header_keys(self):
        r"""List Header Keys."""
        keys = list(set(self.hdr.keys()))
        return keys


    @property
    def centerpixel(self):
        rownum = int(self.data.shape[0] / 2.0)
        colnum = int(self.data.shape[1] / 2.0)
        return (rownum, colnum)

    @property
    def shape(self):
        return self.data.shape

    def dist_from_center(self, row, col):
        r"""
        """

    def pix_from_center(self, row, col):
        r"""
        """
        center_row, center_col = self.centerpixel
        delta_row = row - center_row
        delta_col = col - center_col
        return delta_row, delta_col

    def distpix_from_pix(self, delta_row, delta_col, sampling_factor=1):
        r"""
        """
        if isinstance(sampling_factor, int):
            sampling_factor = (sampling_factor, sampling_factor)
        psfcenrow, psfcencol = self.center

        # Determining the pixel location of the larger PSF array that
        # needs to be the center of the array so that the PSF center is
        # shifted properly. Note: if the sampling of the super-sampled PSF
        # changes this variable will need to be changed. At this time it
        # is 4 times the sampling.
        delta_psf_row = psfcenrow - delta_row * sampling_factor[0]
        delta_psf_col = psfcencol - delta_col * sampling_factor[1]
        return int(delta_psf_row), int(delta_psf_col)



    def trim(self, tlims=[[50, 4110], [950, 1200]], prefix='t'):
        r''' Trims an image

        Parameters
        -----------
        tlims: list
            The limits of the trim region.. The format should be:
            [[<lower_x_lim>, <upper_x_lim>], [<lower_y_lim>, <upper_y_lim>]]

        prefix: str
            Prefix of the output corrected image, if save=True. The corrected image will
            be placed on the same folder of the original fitsfile, with the prefix on the
            basename.

        Returns
        --------
        Trimmed Image object
        '''
        img = copy(self)
        # cutting image array
        img.data = self.data[tlims[1][0]:tlims[1][1], tlims[0][0]:tlims[0][1]]
        if prefix is not None:
            label = prefix + self.label
        img.hdr['PEDRA_TRIM'] = tlims.__repr__()
        if self.err is not None:
            img.err = self.err[tlims[1][0]:tlims[1][1], tlims[0][0]:tlims[0][1]]
        return img

 
    def rebin(self, binsize=12,  prefix='r', func=np.sum): # -> implement copy img
        r''' ...
        '''
        img = copy(self)
        img.data = rebin.rebin(self.data, binsize, func=func)
        img.hdr['PEDRA_REBIN'] = str(binsize)
        if prefix is not None:  
            img.label = prefix + img.label
        if self.err is not None:
            img.err = rebin.rebin(self.err, binsize, func=func)
        return img

    def normalize(self, label='nflat.fits', metric='median', axis=0, order=5, plotfit=True): # -> implement other metrics, like mean and polynomial fitting
        r''' Normalizes flat field

        Parameters
        ----------
        outlabel: str
            Output file. If save=True

        save: boolean
            True if want to save image.

        order: int
            Order of the polynomial to fit and normalize flat field

        plotfit: boolean
            If want to display the fit on the flat

        Returns
        --------
        Normalized Image object
        '''
        img = copy(self)
        if metric == 'median':
            aux = np.median(img.data, axis=axis)
        if metric == 'sum':
            aux = np.sum(img.data, axis=axis)
        img.data = img.data / aux
        # aux_x = range(len(flat.arr[0]))
        # fit = np.polyfit(aux_x, aux, order)
        # ffit = np.polyval(fit, aux_x)
        # if plotfit:
        #     plt.plot(aux_x, aux, c='b', label='flat')
        #     plt.plot(aux_x, ffit, c='r', label='fit')
        #     plt.legend(loc='lower right')
        # # normalizing
        # flat.arr = np.divide(flat.arr, ffit)
        img.label = label
        # producing new image
        # if plotimg:
        #     flat.view()

        return img

    def saveimg(self, outdir='./', outfile=None, data_ext=1, hdr_ext=0, wcs_ext=1):
        r''' Save fits image.

        Parameters
        ----------
        outfile: str
            Name of the new image file
        '''
        # Insert value in header to motify last change on file
        self.hdr['PEDRA_EDIT'] = str(datetime.datetime.now())
        hdu = fits.HDUList()
        hdu.append(fits.PrimaryHDU(self.data, header=self.hdr))
        if self.wcs is not None:
            hdu.append(fits.ImageHDU(self.data, header=self.wcs.to_header()))

        if outfile is None:
            outfile = self.label
        if outfile[-5:] != '.fits':
            outfile += '.fits'
        hdu.writeto(outdir+outfile, overwrite=True)

    def divide(self, other):
        r''' Divide images element-wise.

        Parameters
        ----------
        other: Image object
            Image dividend

        Returns
        -------
        The images division, element-wise
        '''
        img = copy(self)
        assert len(self.data) == len(other.data)
        img.data = np.divide(self.data, other.data)
        return img

    def multiply(self, other):
        r''' Multiply images element-wise.

        Parameters
        ----------
        other: Image object
            Image multiplier

        Returns
        -------
        The product of the image by the other image, element-wise
        '''
        img = copy(self)
        assert len(self.data) == len(other.data)
        img.data = np.multiply(self.data, other.data)
        return img

    def make_mask(self, val=0, mask_err=True): # -> rever isso daqui
        r"""
        """
        # if (not mask_err) | (self.err is None):
        #     mask = np.mask_where(img.data == 0, img.data, copy=False)
        # else:
        #     mask = np.mask_where((img.data == 0) | (img.err == 0),
        #                          img.data, copy=False)
        # self.mask = mask

    def __sub__(self, other):
        img = copy(self)
        img.data = self.data - other.data
        return img

    def __add__(self, other):
        img = copy(self)
        img.data = self.data + other.data
        return img

    def __div__(self, val):
        img = copy(self)
        img.data = self.data / val
        return img

    def __mul__(self, val):
        img = copy(self)
        img.data = self.data * val
        return img


    def view(self, ax=None, show=False,
                savefig=False, figdir='.', **kwargs):
            r''' Display image

            Parameters
            ----------
            fax (Optional): None, matplotlib.axes
                If desired to subplot image in a figure. Default is 'None', which
                will open a new plt.figure()

            show (Optional): boolean
                True if want to plt.show(). Default is True.

            savefig (Optional): boolean
                True if want to save image.

            figdir (Optional): str
                Only needed if savefig=True. Directory for saving image.
                The file basename will be the same name of the image fitsfile.
                The image extention is .png.

            **kwargs: matplotlib kwargs

            '''
            # setting default values for image plot with matplotlib
            kwargs_defaults = {'cmap': plt.cm.gray, 
                            'vmin': np.median(self.data) - np.std(self.data),
                            'vmax': np.median(self.data) + 5*np.std(self.data),
                            'origin': 'lower'}
            kwargs = kwargupdate(kwargs_defaults, kwargs)
            # plotting image
            if ax is None:
                fig = plt.figure()
                ax = fig.gca()
            im = ax.imshow(self.data, **kwargs)
            # outputing image
            if savefig:
                plt.savefig(figdir+self.name+'.png')
            if show:
                plt.show()
            return im

    def view_contour(self, ax=None, show=False,
                savefig=False, figdir='.', **kwargs):
            r''' Display image

            Parameters
            ----------
            fax (Optional): None, matplotlib.axes
                If desired to subplot image in a figure. Default is 'None', which
                will open a new plt.figure()

            show (Optional): boolean
                True if want to plt.show(). Default is True.

            savefig (Optional): boolean
                True if want to save image.

            figdir (Optional): str
                Only needed if savefig=True. Directory for saving image.
                The file basename will be the same name of the image fitsfile.
                The image extention is .png.

            **kwargs: matplotlib kwargs

            '''
            # setting default values for image plot with matplotlib
            kwargs_defaults = {'cmap': plt.cm.gray, 
                               'origin': 'lower'}
            kwargs = kwargupdate(kwargs_defaults, kwargs)
            # plotting image
            if ax is None:
                fig = plt.figure()
                ax = fig.gca()
            ax.contourf(self.data, **kwargs)
            # outputing image
            if savefig:
                plt.savefig(figdir+self.name+'.png')
            if show:
                plt.show()
