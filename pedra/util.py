
from datetime import datetime
import numpy as np
from astropy.io import fits
from astroquery.jplhorizons import Horizons
from astropy.time import Time

from .core import Image
from .cubes import Cube
from .reduc import flatcorrect

def get_ephem(location='@hst',
              target='CERES', date='2024-01-01', 
              time='00:00:00.0', 
              dateformat='%Y-%m-%d %H:%M:%S',
              values=['RA', 'DEC', 'V']):
    r"""
    """
    # Asteroid name
    name = target.strip().upper().replace('-', ' ')
    # Time
    date = f"{date} {time}"
    date = datetime.strptime(date, dateformat)
    jd = Time(date).jd

    hor = Horizons(id=name,  location=location,
                    epochs=[jd])
    ephem = hor.ephemerides()
    if values is not None:
        ephem = ephem[values]
    return ephem

def get_ephem_header(img, location='@hst',
              targetkey='TARGNAME', datekey='DATE-OBS', 
              timekey='TIME-OBS', exptimekey='EXPTIME', 
              dateformat='%Y-%m-%d %H:%M:%S',
              values=['RA', 'DEC', 'V']):
    r"""
    """
    # Asteroid name
    name = img.hdr[targetkey].strip().upper().replace('-', ' ')
    # Time
    if timekey is None:
        date = f"{img.hdr[datekey].strip()}"
    else:
        date = f"{img.hdr[datekey].strip()} {img.hdr[timekey].strip()}"
    date = datetime.strptime(date, dateformat)
    jd = Time(date).jd

    hor = Horizons(id=name,  location=location,
                    epochs=[jd])
    ephem = hor.ephemerides()
    if values is not None:
        ephem = ephem[values]
    return ephem

def align_and_trim(img1, img2, row, col, sampling_factor=4): 
    r"""
    Only working for jwst 4 times res#!!!!!!
    """
    assert (row + img2.shape[0]) * sampling_factor <= img1.shape[0]
    assert (row + img2.shape[1]) * sampling_factor <= img1.shape[1]
    img_row, img_col = img2.center
    # img_delta_row, img_delta_col = img2.pix_from_center(row, col)
    cen_row, cen_col = img1.center
    delta_cen_row = cen_row  + (img_row - row)*sampling_factor +0.5# * sampling_factor) 
    delta_cen_col = cen_col  + (img_col - col)*sampling_factor +0.5 #* sampling_factor) 
    # Now determining the size of the rows and columns of the super
    # sampled PSF aray need to be kept.
    rownum = img2.data.shape[0] * (sampling_factor/2) 
    colnum = img2.data.shape[1] * (sampling_factor/2) 
    init_row = int(np.round((delta_cen_row - rownum) + (0.5 *sampling_factor)))
    end_row = int(np.round(init_row + (sampling_factor*img2.data.shape[1])))
    init_col = int(np.round((delta_cen_col - colnum) + (0.5 *sampling_factor))) 
    end_col = int(np.round(init_col + sampling_factor * img2.shape[0]))
    # Shift the center of the PSF within the image by trimming pixels
    # on the borders (both row and col).
    psf_cut = img1.trim([[init_row, end_row],
                        [ init_col, end_col]])
    return psf_cut

def group_by(imglist, by='FILTER2'):
    r"""
    """
    imgs_group = {}
    for img in imglist:
        if img.hdr[by] in imgs_group.keys():
            imgs_group[img.hdr[by]].append(img)
        else:
            imgs_group[img.hdr[by]] = [img]         
    return imgs_group


def combine_groups(imgdict, label='combine', metric='mean', axis=0):
    r"""
    """
    combines = {}
    for fil, imgs in imgdict.items():
        combines[fil] = combine(imgs, label, metric, axis)
    return combines


def normalize_groups(imgdict, label='nflat.fits', metric='median', axis=0, order=5, plotfit=True):
    nimgs= {}
    for fil, img in imgdict.items():
        nimgs[fil] = img.normalize(label='nflat.fits', metric='median', axis=0, order=5, plotfit=True)
    return nimgs


def flatcorrect_groups(imgdict, flatdict):
    fimgs = {x:imgdict[x] for x in imgdict.keys()}
    for fil, imgs in imgdict.items():
        if isinstance(imgs, list):
            for i, im in enumerate(imgs):
                fimgs[fil][i] = flatcorrect(im, flatdict[fil])
        else:
            fimgs[fil] = flatcorrect(im, flatdict[fil])
    return fimgs


def combine(imglist, label='combine', metric='median', axis=0):
    r''' Combines a list of images using the mean or the median.

    Parameters
    -----------
    imglist: list
        List of fits file to be combined

    label: str
        Output file. If save=True

    metric: str
        Method of combination. Default is the mean.
        Options: Mean, Median

    Returns
    --------
    Combined Image object
    '''
    imgs_arr = [fits.data for fits in imglist]
    # applying combination method
    if metric == 'mean':
        img_combine = np.mean(imgs_arr, axis=axis)
    if metric == 'median':
        img_combine = np.median(imgs_arr, axis=axis)
    if metric == 'sum':
        img_combine = np.sum(imgs_arr, axis=axis)
    # producing new image
    img = Image(img_combine, imglist[0].hdr, wcs=imglist[0].wcs, label=label)
    img.hdr['PEDRA_COMBINE'] = 'Combined image'
    img.hdr['PEDRA_COMBINE_METRIC'] = metric
    return img


def read_jwst_psf(fpath):
    r"""
    """
    models = fits.open(fpath)
    psf_model = models["OVERSAMP"].data
    psf_header = models["OVERSAMP"].header
    psf_cube = Cube(psf_model, psf_header, label='JWST NIRSpec PSF')
    return psf_cube