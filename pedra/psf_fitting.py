
import numpy as np
import dynesty
import functools
import wquantiles
from multiprocessing import Pool

def prior_flux(u):
    return u

def chi2(data, model, unc):
    r"""Calculate the chi-square."""
    res_aux = (data - model)**2
    err = 1. / unc**2
    return -0.5*(np.sum((res_aux*err) - np.log(err)))

def chi_slice_flux(params, img, psf_img):
    r"""
    """
    flux, bckg = params
    model = flux * psf_img.data + bckg
    # Finding values of nan in the arrays and removing them. I need to
    # flatten the arrays first for this.
    data = img.data.flatten()
    model = model.flatten() 
    unc = img.err.flatten()
    # Incase there are regions in the err array that are nan but are
    # not nan in the data array.
    nanindices = np.where(data != 0)
    nanindices_err = np.where(unc[nanindices] != 0)

    data = data[nanindices][nanindices_err]
    model = model[nanindices][nanindices_err]
    unc = unc[nanindices][nanindices_err]
    
    nanindices = np.where(data != 0)
    # Determining the number of free variables in the fitting
    # chi-squared calculation. This will be the number of slice array
    # locations that are not a value of 'nan'. Meaning, these
    # locations have real data.
    numpix = data.size
    # Returning the reduced chi-squared statistic.
    # chi2 = (1.0 / (numpix - 4.0)) * np.sum(((data - model) / unc) ** 2)
    c2 = chi2(data, model, unc)
    return c2


def fit_slice_flux(slice, cube, psf_cube):
    r"""
    """
    loglike = functools.partial(chi_slice_flux, img=cube.select_slice(slice), psf_img=psf_cube.select_slice(slice))
    m = dynesty.DynamicNestedSampler(loglike, prior_flux, 2, nlive=50, bound='single', sample='rslice')
    m.run_nested(print_progress=False)
    flux = wquantiles.quantile_1D(m.results.samples[:,0], m.results.importance_weights(), (0.5))
    bkg = wquantiles.quantile_1D(m.results.samples[:,1], m.results.importance_weights(), (0.5))
    return slice, flux, bkg


def fit_cube_flux(cube, psf_cube):
    r"""
    """
    p = Pool(10)
    fit_slice_flux_aux = functools.partial(fit_slice_flux, cube=cube, psf_cube=psf_cube)
    res = p.map(fit_slice_flux_aux, range(cube.nslices))
    return res