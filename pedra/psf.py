
import numpy as np
from numpy import ma
from dynesty import NestedSampler
import functools
import wquantiles
from multiprocessing import Pool
from copy import copy

from .util import align_and_trim
from .cubes import Cube
# def prior_flux(u):
#     u[0] = u[0] * 1e-5
#     u[1] = u[1] * 1e-9
#     u[2] = 8 * u[2] + 22  # scale and shift to [-1., 1.)
#     u[3] = 8 * u[3] + 22  # scale and shift to [-1., 1.)
   
#     u[2:] = u[2:]
#     return u


# def chi2(data, model, unc):
#     r"""Calculate the chi-square."""
#     res_aux = (data - model)**2
#     err = 1. / data**2
#     return -0.5*(np.sum(res_aux*err))

# def chi_slice(params, img, psf_img):
#     r"""
#     """
#     flux, bckg, row, col = params
#     img_new = align_and_trim(psf_img, img, row, col)
#     img_new = img_new.rebin(4)
#     model = flux * img_new.data + bckg
#     # Finding values of nan in the arrays and removing them. I need to
#     # flatten the arrays first for this.
#     data = img.data.flatten()
#     model = model.flatten() 
#     unc = img.err.flatten()
#     # Incase there are regions in the err array that are nan but are
#     # not nan in the data array.
#     nanindices = np.where(data != 0)
#     nanindices_err = np.where(unc[nanindices] != 0)

#     data = data[nanindices][nanindices_err]
#     model = model[nanindices][nanindices_err]
#     unc = unc[nanindices][nanindices_err]
    
#     nanindices = np.where(data != 0)
#     # Determining the number of free variables in the fitting
#     # chi-squared calculation. This will be the number of slice array
#     # locations that are not a value of 'nan'. Meaning, these
#     # locations have real data.
#     numpix = data.size
#     # Returning the reduced chi-squared statistic.
#     # chi2 = (1.0 / (numpix - 4.0)) * np.sum(((data - model) / unc) ** 2)
#     c2 = chi2(data, model, unc)
#     return c2


def prior_flux(u):
    r'''
    '''
    u[0] = 2* u[0] * 1e-8
    u[1] = u[1] * 2e-10 - 1e-10
    # u[2] = 8 * u[2] + 20  # scale and shift to [-1., 1.)
    # u[3] = 8 * u[3] + 20  # scale and shift to [-1., 1.)
    return u

def chi2(data, model):
    r"""Calculate the chi-square."""
    # inv_err = np.linalg.pinv(img.err).T
    masked_data = ma.masked_where((data.data==0) | (data.err==0)| (model.data==0), 
                                  data.data, copy=True)
    masked_model = ma.masked_array(model.data, mask= masked_data.mask)
    masked_err = ma.masked_array(data.err, mask= masked_data.mask)
    res_aux = (masked_data - masked_model)**2
    err = (1/masked_err)**2 
    return -0.5*(np.sum((res_aux*err)- np.log(err)))

    
def chi_slice(params, img, img_psf):
    r"""
    Calculates the log-likelihood based on the chi-squared statistics.

    Parameters
    ----------
    params: (float, float) 
        The (Flux, Background) values.

    img: pedra.Image
        Science Image object.
    
    img_psf: pedra.Image
        PSF Image object to be fitted.
    
    Returns
    -------
    float
        The chi-squared statistics.
    """
    flux, bkg = params
    # img_new = pedra.align_and_trim(img_psf, img, row, col, sampling_factor=1)
    # img_new = img_new.rebin(1)
    model = copy(img_psf)
    model.data = flux * img_psf.data + bkg

    c2 = chi2(img, model)
    return c2

def fit_image_flux(img, img_psf, quantiles=(0.16, 0.5, 0.84), 
                   sampling_kwargs=None, running_kwargs=None,
                   prior_kwargs=None):
    # Defining the likelihood
    loglike = functools.partial(chi_slice, img=img, img_psf=img_psf)
    # Setting and running Nested Sampling
    model = NestedSampler(loglike, prior_flux, 2, nlive=30, bound='single', sample='rwalk')
    model.run_nested(print_progress=False, dlogz=1, maxiter=1000)
    # Getting Flux best-fit
    flux = [wquantiles.quantile_1D(model.results.samples[:,0], 
                                  model.results.importance_weights(), 
                                  q) for q in quantiles]
    #Getting Background best-fit
    bkg = [wquantiles.quantile_1D(model.results.samples[:,1], 
                                 model.results.importance_weights(), 
                                  q) for q in quantiles]
    # # Getting Background best-fit
    # row = [wquantiles.quantile_1D(model.results.samples[:,2], 
    #                              model.results.importance_weights(), 
    #                               q) for q in quantiles]
    # # Getting Background best-fit
    # col = [wquantiles.quantile_1D(model.results.samples[:,3], 
    #                              model.results.importance_weights(), 
    #                               q) for q in quantiles]
    return flux , bkg#, row, col


def fit_slice_flux(slc, cube, psf_cube='make', size=30, offset=15, quantiles=(0.16, 0.5, 0.84),
                   stacking_kwargs=None, sampling_kwargs=None, running_kwargs=None):
    r"""
    Fit the Slice to the PSF.

    Parameters
    ----------
    slc: int
        The Slice of the data cube to be fitted. 

    cube: pedra.Cube
        Science cube object

    psf_cube: str, pedra.Cube
        The datacube containing the PSF information. If psf_cube=False (default), 
        it will make an empirical PSF by stacking slices and calculating their median.

    size: int
        Size of the stacking. Only used if psf_cube=False.
        Default is size=30.

    offset: int
        Size of the offset to start the stacking. Default is 15.
        E.g. if evaluating slice 30, the empirical PSF will be the
        median of slices 15 to 45. Only used if psf_cube=False.

    quantiles: list
        List of quantiles to be used to calculate the best fit and uncertainties.
        Default is (0.16, 0.5, 0.84), which will take the mean and one-sigma values 
        from the posterior.
    
    parallel: boolean
        True if want to parallelize the fitting of the slices.
        Default is True.
    
    queue_size: int
        Number of CPUs to be used. Only used it parallel=True.
        Default is 6.

    stacking_kwargs: dict, None

    sampling_kwargs: dict, None

    running_kwargs: dict, None


    Returns
    -------
    (slc, flux, flux_err, bkg, bkg_err)
        slc(int): slice number 
        flux(float): best-fit flux
        flux_err(float): fitted flux uncertainty (accordingly to the quantiles)
        bkg(float): best-fit background
        bkg_err(float): fitted background uncertainty (accordingly to the quantiles)
    """
    img=cube.select_slice(slc)
    if isinstance(psf_cube, Cube):
        img_psf = psf_cube.select_slice(slc)
    else:
        start = slc - offset
        if start < 0 :
            start = 0
        if start + size >= cube.nslices:
            start = cube.nslices - size -1
        print(slc)
        scube = cube.stack_slices(start=start, size=size, method='median')
        img_psf = scube.select_slice(0)
        img_psf.data[img_psf.data!=0] = img_psf.data[img_psf.data!=0] / img_psf.data.max() \
                                        - np.median(img_psf.data[img_psf.data!=0])
        # img_psf.data = img_psf.data /np.sum(img)
    out = fit_image_flux(img, img_psf, quantiles)
    return slc, out


def fit_cube(cube, psf_cube=False, size=30, offset=15, quantiles=(0.16, 0.5, 0.84), 
             parallel=True, queue_size=6, stacking_kwargs=None, sampling_kwargs=None, 
             running_kwargs=None):
    r"""
    Fit the PSF cube to the Science cube to extract the flux and backgroud of each slice.

    Parameters
    ----------
    cube: pedra.Cube
        Science cube object

    psf_cube: str, pedra.Cube
        The datacube containing the PSF information. If psf_cube=False (default), 
        it will make an empirical PSF by stacking slices and calculating their median.

    size: int
        Size of the stacking. Only used if psf_cube=False.
        Default is size=30.

    offset: int
        Size of the offset to start the stacking. Default is 15.
        E.g. if evaluating slice 30, the empirical PSF will be the
        median of slices 15 to 45. Only used if psf_cube=False.

    quantiles: list
        List of quantiles to be used to calculate the best fit and uncertainties.
        Default is (0.16, 0.5, 0.84), which will take the mean and one-sigma values 
        from the posterior.
    
    parallel: boolean
        True if want to parallelize the fitting of the slices.
        Default is True.
    
    queue_size: int
        Number of CPUs to be used. Only used it parallel=True.
        Default is 6.

    stacking_kwargs: dict, None

    sampling_kwargs: dict, None

    running_kwargs: dict, None


    Returns
    -------
    cana.Spectrum
        Cana Spectrum object with the fitted flux and wavelength for each slice.
    """
    fit_slice_flux_aux = functools.partial(fit_slice_flux, cube=cube,
                                           psf_cube=psf_cube,  size=size, 
                                           offset=offset, quantiles=quantiles,
                                           stacking_kwargs=stacking_kwargs, 
                                           sampling_kwargs=sampling_kwargs, 
                                           running_kwargs=running_kwargs)
    if parallel:
        pool = Pool(queue_size)
        res = pool.map(fit_slice_flux_aux, range(cube.nslices)[:])
    else:
        res = list(map(fit_slice_flux_aux, range(cube.nslices)[:]))
    return res