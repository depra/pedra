
import matplotlib.pyplot as plt
import numpy as np
from astropy.coordinates import SkyCoord
from astropy import units as u
import twirl
from photutils.detection import DAOStarFinder
from astropy.stats import sigma_clipped_stats
import pandas as pd
from scipy.spatial import KDTree

def nearest_source(sources, point):
    r"""
    """
    source_arr = sources[['xcentroid', 'ycentroid']].to_numpy()
    source_tree = KDTree(source_arr)
    source_tree.query([800, 800])

def detect_sources(img, fwhm=3, threshold=5, sigma=3, sort_by='flux', nsources=None): #-> not working properly
    r"""
    """
    mean, median, std = sigma_clipped_stats(img.data, sigma=sigma)
    daofind = DAOStarFinder(fwhm=fwhm, threshold=threshold*std)  
    sources = daofind(img.data - median)
    sources.sort(sort_by, reverse=True)
    sources = sources.to_pandas()
    if nsources is not None:
        sources = sources.iloc[:nsources]
    return sources


def detect_sources_cube(cube, fwhm=3, threshold=5, sigma=3, sort_by='flux', nsources=1): #-> not working properly
    aux = []
    for i in range(cube.nslices):
        # print(i)
        s = detect_sources(cube.select_slice(i), fwhm=fwhm, threshold=threshold,
                           sigma=sigma, sort_by=sort_by, nsources=nsources)
        # print(len(s))
        aux.append(s)
        
    cubesources = pd.concat(aux, ignore_index=True)
    cubesources['slice'] = cubesources.index
    return cubesources


def solve_plate(img, ra, dec, fov, 
                limit=15, n=15, unit=["h", "deg"],
                return_wcs=True, write_image=True):  #-> not working properly
    center = SkyCoord(ra, dec, unit=unit)
    center = [center.ra.value, center.dec.value]
    stars = twirl.find_peaks(img.arr)[0:n]
    gaias = twirl.gaia_radecs(center, fov/2, limit=limit)
    if n is None:
        n = len(stars)
    wcs = twirl._compute_wcs(stars[:n], gaias, n=n)
    if return_wcs:
        return wcs


def cross_catalog():
    """
    """
