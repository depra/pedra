import customtkinter as ctk
import matplotlib.pyplot as plt
from scipy.spatial import KDTree
import photutils
import numpy as np
from pedra.calib  import detect_sources
from pedra.util import get_ephem
# class DetectFrame(ctk.CTkFrame):

#     def __init__(self, master, viewer, scenes, **kwargs):

class AsteroidIdentifyFrame(ctk.CTkFrame):
    
    def __init__(self, master, viewer, scenes, info, **kwargs):
        self.asteroid_plot = False
        self.asteroid = ""
        self.date = ""
        self.time = ""
        self.asteroid_info = None
        super().__init__(master, **kwargs)
        self.viewer = viewer
        self.scenes = scenes
        self.info = info
        # Asteroid
        self.target_frame = ctk.CTkFrame(self)
        self.target_label = ctk.CTkLabel(self.target_frame,
                                           text="Target:")
        self.target_label.pack(side=ctk.LEFT, padx=5)
        self.target_entry = ctk.CTkEntry(self.target_frame, 
                                           placeholder_text="",
                                           width=50)
        self.target_entry.insert(0, "")
        self.target_entry.pack(side=ctk.LEFT, padx=5)
        self.target_value_label = ctk.CTkLabel(self.target_frame, text=self.target_entry.get())
        self.target_value_label.pack(side=ctk.LEFT, padx=5)
        self.target_entry.bind('<Return>', lambda event=None: self.update_target())
        self.target_frame.pack(pady=5)
        # Date
        self.date_frame = ctk.CTkFrame(self)
        self.date_label = ctk.CTkLabel(self.date_frame,
                                           text="Date:")
        self.date_label.pack(side=ctk.LEFT, padx=5)
        self.date_entry = ctk.CTkEntry(self.date_frame, 
                                           placeholder_text="",
                                           width=50)
        self.date_entry.insert(0, "")
        self.date_entry.pack(side=ctk.LEFT, padx=5)
        self.date_value_label = ctk.CTkLabel(self.date_frame, text=self.date_entry.get())
        self.date_value_label.pack(side=ctk.LEFT, padx=5)
        self.date_entry.bind('<Return>', lambda event=None: self.update_date())
        self.date_frame.pack(pady=5)
        # Time        
        self.time_frame = ctk.CTkFrame(self)
        self.time_label = ctk.CTkLabel(self.time_frame,
                                           text="Time:")
        self.time_label.pack(side=ctk.LEFT, padx=5)
        self.time_entry = ctk.CTkEntry(self.time_frame, 
                                           placeholder_text="",
                                           width=50)
        self.time_entry.insert(0, "")
        self.time_entry.pack(side=ctk.LEFT, padx=5)
        self.time_value_label = ctk.CTkLabel(self.time_frame, text=self.time_entry.get())
        self.time_value_label.pack(side=ctk.LEFT, padx=5)
        self.time_entry.bind('<Return>', lambda event=None: self.update_time())
        self.time_frame.pack(pady=5)
        # Time        
        self.obs_frame = ctk.CTkFrame(self)
        self.obs_label = ctk.CTkLabel(self.obs_frame,
                                           text="Obs code:")
        self.obs_label.pack(side=ctk.LEFT, padx=5)
        self.obs_entry = ctk.CTkEntry(self.obs_frame, 
                                           placeholder_text="@hst",
                                           width=50)
        self.obs_entry.insert(0, "@hst")
        self.obs_entry.pack(side=ctk.LEFT, padx=5)
        self.obs_value_label = ctk.CTkLabel(self.obs_frame, text=self.obs_entry.get())
        self.obs_value_label.pack(side=ctk.LEFT, padx=5)
        self.obs_entry.bind('<Return>', lambda event=None: self.update_obs())
        self.obs_frame.pack(pady=5)
        ## Target Identify
        self.target_identify_buttom = ctk.CTkButton(self, 
                                                    text='Get Ephemerides', 
                                                    command=self.asteroid_ephem)
        self.target_identify_buttom.pack(padx=20, pady=20)

        #  Show sources
        self.show_asteroid_check = ctk.CTkCheckBox(self, text="Show Asteroid", command=self.show_asteroid_select)
        self.show_asteroid_check.select()
        self.show_asteroid_check.pack(pady=5)
        # Bindings
        self.scenes.current.openfile.bind('<Button-1>', self.update_params)
        self.scenes.img_tabview.configure(command=self.alternate_scene)

    def alternate_scene(self):
        r"""
        """
        self.scenes.alternate_scene()
        if self.scenes.current.fitsimg is not None:
            self.update_params(0)
    
    def asteroid_ephem(self):
        r"""
        """
        ephem = get_ephem(target=self.target_entry.get(),
                          location=self.obs_entry.get(),
                           date=self.date_entry.get(), 
                           time=self.time_entry.get(), 
                           dateformat='%Y-%m-%d %H:%M:%S',
                           values=['RA', 'DEC', 'V'])
        x, y = self.scenes.current.fitsimg.wcs.world_to_pixel_values([[ephem['RA'][0], ephem['DEC'][0]]])[0]
        self.asteroid_info = {'x':x, 
                              'y': y,
                              'ra': ephem['RA'][0],
                              'dec': ephem['DEC'][0],
                              'v': ephem['V'][0] }
        if self.show_asteroid_check.get():
            self.hide_asteroid()
            self.show_asteroid()
        self.update_info()

    def update_info(self):
        r"""
        """
        if self.asteroid_info is not None:
            self.info.raast = np.round(self.asteroid_info['ra'], decimals=4)
            self.info.decast = np.round(self.asteroid_info['dec'], decimals=4)
            self.info.xast = np.round(self.asteroid_info['x'], decimals=2)
            self.info.yast = np.round(self.asteroid_info['y'], decimals=2)
            self.info.vast = np.round(self.asteroid_info['v'], decimals=2)
            self.info.update_text()

    def update_params(self, event):
        r"""
        """
        # if self.target_entry.get() == "":
        self.target_entry.delete(0, "end")
        self.target_entry.insert(0, self.scenes.current.tar_key_value_label.cget("text"))
        self.date_entry.delete(0, "end")
        self.date_entry.insert(0, self.scenes.current.date_key_value_label.cget("text"))
        self.time_entry.delete(0, "end")
        self.time_entry.insert(0, self.scenes.current.time_key_value_label.cget("text"))
        self.update_date()
        self.update_target()
        self.update_time()
        self.update_obs()

    def update_target(self):
        r"""
        """
        value = self.target_entry.get()
        self.asteroid = value
        self.target_value_label.configure(text=f"{value}")
        self.target_value_label.configure(text=f"{value}")

    def update_obs(self):
        r"""
        """
        value = self.obs_entry.get()
        self.obs = value
        self.obs_value_label.configure(text=f"{value}")

    def update_date(self):
        r"""
        """
        value = self.date_entry.get()
        self.date = value
        self.date_value_label.configure(text=f"{value}")

    def update_time(self):
        r"""
        """
        value = self.time_entry.get()
        self.time = value
        self.time_value_label.configure(text=f"{value}")

    def show_asteroid(self):
        r"""
        """
        self.hide_asteroid()
        self.asteroid_plot = self.viewer.ax.scatter(self.asteroid_info['x'],
                                                    self.asteroid_info['y'],
                                                    marker='+', color='purple',
                                                    s=50)
        self.viewer.canvas.draw()

    def hide_asteroid(self):
        r"""
        """
        if self.asteroid_plot:
            self.asteroid_plot.remove()
            self.asteroid_plot = False
        
    def show_asteroid_select(self):
        r"""
        """
        if self.show_asteroid_check.get():
            if self.asteroid_info:
                self.show_asteroid()
        else:
            self.hide_asteroid()
            self.viewer.canvas.draw()

class ZeroPointFrame(ctk.CTkFrame):

    def __init__(self, master, viewer, scenes, info, **kwargs):
        self.sources = None
        self.sources_plot = None
        super().__init__(master, **kwargs)


class DetectSourcesFrame(ctk.CTkFrame):
    
    def __init__(self, master, viewer, scenes, info, **kwargs):
        self.sources = None
        self.sources_arr = None
        self.sources_tree = None
        self.sources_plot = False
        super().__init__(master, **kwargs)
        self.viewer = viewer
        self.scenes = scenes
        self.info = info

        ## Detection params
        # FWHM
        self.fwhm_frame = ctk.CTkFrame(self)
        self.fwhm_label = ctk.CTkLabel(self.fwhm_frame,
                                           text="FWHM:")
        self.fwhm_label.pack(side=ctk.LEFT, padx=5)
        self.fwhm_entry = ctk.CTkEntry(self.fwhm_frame, 
                                           placeholder_text="3",
                                           width=50)
        self.fwhm_entry.insert(0, "3")
        self.fwhm_entry.pack(side=ctk.LEFT, padx=5)
        self.fwhm_value_label = ctk.CTkLabel(self.fwhm_frame, text=self.fwhm_entry.get())
        self.fwhm_value_label.pack(side=ctk.LEFT, padx=5)
        self.fwhm_entry.bind('<Return>', lambda event=None: self.update_fwhm())
        self.fwhm_frame.pack(pady=5)
        # Threshold
        self.threshold_frame = ctk.CTkFrame(self)
        self.threshold_label = ctk.CTkLabel(self.threshold_frame,
                                           text="Threshold:")
        self.threshold_label.pack(side=ctk.LEFT, padx=5)
        self.threshold_entry = ctk.CTkEntry(self.threshold_frame, 
                                           placeholder_text="5",
                                           width=50)
        self.threshold_entry.insert(0, "5")
        self.threshold_entry.pack(side=ctk.LEFT, padx=5)
        self.threshold_value_label = ctk.CTkLabel(self.threshold_frame, text=self.threshold_entry.get())
        self.threshold_value_label.pack(side=ctk.LEFT, padx=5)
        self.threshold_entry.bind('<Return>', lambda event=None: self.update_threshold())
        self.threshold_frame.pack(pady=5)

        # N sources
        self.nsources_frame = ctk.CTkFrame(self)
        self.nsources_label = ctk.CTkLabel(self.nsources_frame,
                                           text="N Sources:")
        self.nsources_label.pack(side=ctk.LEFT, padx=5)
        self.nsources_entry = ctk.CTkEntry(self.nsources_frame, 
                                           placeholder_text="15",
                                           width=50)
        self.nsources_entry.insert(0, "15")
        self.nsources_entry.pack(side=ctk.LEFT, padx=5)
        self.nsources_value_label = ctk.CTkLabel(self.nsources_frame, text=self.nsources_entry.get())
        self.nsources_value_label.pack(side=ctk.LEFT, padx=5)
        self.nsources_entry.bind('<Return>', lambda event=None: self.update_nsources())
        self.nsources_frame.pack(pady=5)
    
        #  Detect Sources button
        self.detect_button = ctk.CTkButton(self,  text='Detect Sources', command=self.detect_sources_act)
        self.detect_button.pack(pady=5)

        #  Show sources
        self.show_sources_check = ctk.CTkCheckBox(self, text="Show Sources", command=self.show_sources_select)
        self.show_sources_check.select()
        self.show_sources_check.pack(pady=5)

        #  Track sources
        self.track_sources = ctk.CTkCheckBox(self, text="Track Sources", command=None)
        self.track_sources.select()
        self.track_sources.pack(pady=5)

    def show_sources_select(self):
        r"""
        """
        if self.show_sources_check.get():
            if self.sources is not None:
                self.show_sources()
        else:
            if self.sources_plot:
                self.sources_plot.remove()
                self.sources_plot = False
                self.viewer.canvas.draw()

    def nearest_source(self, value):
        r"""
        """
        distance, source_ind = self.sources_tree.query(value)
        return self.sources_arr[source_ind]

    def update_fwhm(self):
        r"""
        """
        value = self.fwhm_entry.get()
        self.fwhm_value_label.configure(text=f"{value}")
        
    def update_threshold(self):
        r"""
        """
        value = self.threshold_entry.get()
        self.threshold_value_label.configure(text=f"{value}")

    def update_nsources(self):
        r"""
        """
        value = self.nsources_entry.get()
        self.nsources_value_label.configure(text=f"{value}")

    def update_params(self):
        self.update_fwhm()
        self.update_nsources()
        self.update_threshold()

    def detect_sources_act(self):
        self.sources = None
        if self.scenes.current.fitsimg is not None:
            self.sources = detect_sources(self.scenes.current.fitsimg,
                                          fwhm=float(self.fwhm_entry.get()),
                                          threshold=float(self.threshold_entry.get()),
                                          nsources=int(self.nsources_entry.get()))
            self.sources_arr = self.sources[['xcentroid', 'ycentroid']].to_numpy()
            self.sources_tree = KDTree(self.sources_arr)
            self.info.nsources = len(self.sources_arr)
            self.info.update_text()
        if self.show_sources_check.get():
            self.remove_sources_plot()
            self.show_sources()

    def show_sources(self):
        if self.sources is not None:
            x = self.sources['xcentroid']
            y = self.sources['ycentroid']
            self.sources_plot = self.viewer.ax.scatter(x, y, s=7, color='darkorange')
            self.viewer.canvas.draw()

    def remove_sources_plot(self):
        if self.sources_plot:
            self.sources_plot.remove()
            self.sources_plot = False
