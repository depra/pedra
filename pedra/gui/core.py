import os
import numpy as np
import matplotlib.pyplot as plt
import customtkinter as ctk
# import tkinter as tk
from photutils.aperture import CircularAnnulus, CircularAperture, aperture_photometry
# from ..core import loadimage
from pedra import loadimage, Image
from pedra.util import get_ephem
from pedra.calib import detect_sources
from imagegui import ImageViewFrame, ImageControlFrame, SceneControlFrame
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
import matplotlib.pyplot as plt
from aperturegui import PhotometryFrame
import pandas as pd


class PlotsViewFrame(ctk.CTkFrame):
    
    def __init__(self, master, **kwargs):
        super().__init__(master, **kwargs)
        self.fig, self.ax = plt.subplots(figsize=(5, 4), dpi=100, facecolor='white')
        # self.fig.set_size_inches(8,7)
        # self.ax.axis("off")
        self.fig.subplots_adjust(left=0.1, right=0.9, bottom=0.1, top=0.9, wspace=0, hspace=0)
        self.canvas = FigureCanvasTkAgg(self.fig, master=self)
        self.toolbar = NavigationToolbar2Tk(self.canvas, self)
        self.toolbar.config(background='white')
        self.toolbar._message_label.config(background='white')
        for button in self.toolbar.winfo_children():
            button.config(background='white')
        self.toolbar.update()
        self.canvas.get_tk_widget().pack(side=ctk.TOP, fill=ctk.BOTH, expand=True)
        self.canvas.draw()
        self.update()


class ResultsFrame(ctk.CTkFrame):
    
    def __init__(self, master, **kwargs):
        self.results_table = None
        super().__init__(master, **kwargs)
        self.store_button = ctk.CTkButton(self, text="Store Photometry", command=None)
        self.store_button.pack(side="right", padx=5)

        self.view_button = ctk.CTkButton(self, text="View Results", command=self.open_results)
        self.view_button.pack(side="right", padx=5)


        self.save_button = ctk.CTkButton(self, text="Save Results", command=self.save_results)
        self.save_button.pack(side="right", padx=5)

    def open_results(self):
        r"""
        """
        if self.results_table is not None:
            # Create a new Tkinter window
            results_window = ctk.CTkToplevel(self)
            results_window.title("Photometric Mesurements")
            results_window.geometry("720x600")

            # Create a Text widget to display text
            results_text = self.results_table.to_markdown()
            results_widget = ctk.CTkTextbox(results_window, wrap="none")
        
            results_widget.insert("0.0", results_text)  
            results_widget.pack(expand=True, fill='both') 
    
    def save_results(self):
        r"""
        """
        if self.results_table is not None:
            file_path = ctk.filedialog.asksaveasfilename(initialfile='results',
                                                         defaultextension=".csv", 
                                                         filetypes=[("CSV files", "*.txt")])
            self.results_table.to_csv(file_path)


class InfoFrame(ctk.CTkFrame):
    
    def __init__(self, master, **kwargs):
        super().__init__(master, **kwargs)
        self.text = ""
        self.image_name = ''
        self.target = ""
        self.date = ""
        self.time = ""
        self.exptime = ""
        self.photconv = ""
        self.zp = ""
        self.nsources = ""
        self.x = ""
        self.y = ""
        self.ra = ""
        self.dec = ""
        self.radius = ""
        self.sky_in = ""
        self.sky_out = ""
        self.count = ""
        self.flux = ""
        self.mag = ""
        self.xast = ""
        self.yast = ""
        self.raast = ""
        self.decast = ""
        self.vast = ""
        self.update_text_string()
        self.info_textbox = ctk.CTkTextbox(self, wrap="word")
        
        self.info_textbox.insert("0.0", self.text)  
        self.info_textbox.pack(expand=True, fill='both') 

    def update_text_string(self):
        r"""
        """
        self.text = f"Image: {self.image_name} \n"\
                     f"-----------------------\n"\
                     f"Taget Name: {self.target}\n"\
                     f"Date: {self.date}\n"\
                     f"Time: {self.time}\n"\
                     f"Exp Time: {self.exptime}\n"\
                     f"Phot Conv Factor: {self.photconv}\n"\
                     f"Zero Point: {self.zp}\n"\
                     f"\n"\
                     f"DETECT SOURCES\n"\
                     f"-----------------------\n"\
                     f"N Detected Sources: {self.nsources}\n"\
                     f"\n"\
                     f"ASTEROID EPHEMERIDES\n"\
                     f"-----------------------\n"\
                     f"X: {self.xast} Y: {self.yast}\n"\
                     f"RA: {self.raast} DEC:{self.decast}\n"\
                     f"V: {self.vast} mag\n"\
                     f"\n"\
                     f"APERTURE PHOTOMETRY\n"\
                     f"-----------------------\n"\
                     f"X: {self.x} Y: {self.y}\n"\
                     f"RA: {self.ra} DEC:{self.dec}\n"\
                     f"Count: {self.count}\n"\
                     f"Flux: {self.flux}\n"\
                     f"Mag: {self.mag}"
        
    def update_text(self):
        self.update_text_string()
        self.info_textbox.delete("0.0", "end")
        self.info_textbox.insert("0.0", self.text)  


class PEDRAAppBase(ctk.CTk):

    def __init__(self):
        super().__init__()

        self.title("PEDRA Aperture Photometry Tool")
        # Image Display Frame
        self.viewer = ImageViewFrame(self, corner_radius=2.5, fg_color="blue")
        ## Image tab
        self.image_tab = ctk.CTkTabview(self)
        self.image_tab.add('Info')
        # Info Display Frame
        self.info = InfoFrame(self.image_tab.tab('Info'), corner_radius=2.5)
        self.info.pack(fill='both', expand=True)
        # Image Control Frame
        self.image_tab.add('Open File')
        self.scenes = SceneControlFrame(self.image_tab.tab('Open File'), 
                                        self.viewer, self.info, corner_radius=2.5)
        self.scenes.pack(fill='both')
        # Plotter Frame
        self.plotter = PlotsViewFrame(self, corner_radius=2.5, fg_color="white")
        # Plotter Frame
        self.results = ResultsFrame(self, corner_radius=2.5)
        # configure grid layout (2x3)
        self.geometry("1280x720")

        self.grid_columnconfigure(0, weight=3)
        self.grid_columnconfigure(1, weight=1)
        self.grid_columnconfigure(2, weight=1)
        self.grid_rowconfigure(0, weight=2)
        self.grid_rowconfigure(1, weight=4)
        self.grid_rowconfigure(2, weight=1)

        self.viewer.grid(row=0, column=0, rowspan=3, sticky="nsew",padx=5, pady=5)
        self.image_tab.grid(row=0, column=1, sticky="nsew",padx=5, pady=5)
        self.plotter.grid(row=0, column=2, sticky="nsew",padx=5, pady=5)
        self.results.grid(row=2, column=1, columnspan=2, sticky="nsew",padx=5, pady=5)




if __name__ == '__main__':
    app = PEDRAAppBase()
    app.mainloop()