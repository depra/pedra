from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
import matplotlib.pyplot as plt
import customtkinter as ctk
import os

from pedra import loadimage

class ImageViewFrame(ctk.CTkFrame):
    
    def __init__(self, master, **kwargs):
        super().__init__(master, **kwargs)
        self.fig, self.ax = plt.subplots(facecolor='black')
        # self.fig.set_size_inches(8,7)
        self.ax.axis("off")
        self.fig.subplots_adjust(left=0, right=1, bottom=0, top=1, wspace=0, hspace=0)
        self.canvas = FigureCanvasTkAgg(self.fig, master=self)
        self.toolbar = NavigationToolbar2Tk(self.canvas, self)
        self.toolbar.config(background='white')
        self.toolbar._message_label.config(background='white')
        for button in self.toolbar.winfo_children():
            button.config(background='white')
        self.toolbar.update()
        self.canvas.get_tk_widget().pack(side=ctk.TOP, fill=ctk.BOTH, expand=1)
        self.canvas.draw()
        self.update()

    


class ImageControlFrame(ctk.CTkFrame):
        
    def __init__(self, master, viewer, info, **kwargs):
        self.fitsimg = None
        self.viewer = viewer
        self.info = info
        self.vmin = 0.1
        self.vmax = 1
        super().__init__(master, **kwargs)
        self.img_tabview = ctk.CTkTabview(self)
        self.img_tabview.add("Fits Config")
        self.img_tabview.add("View Options")
        self.img_tabview.add("Header Keys")
        self.img_tabview.pack(fill='both', pady=5)
         # Data extention
        self.data_ext_frame = ctk.CTkFrame(self.img_tabview.tab("Fits Config"))
        self.data_ext_label = ctk.CTkLabel(self.data_ext_frame,
                                           text="FITS Data Extention:")
        self.data_ext_label.pack(side=ctk.LEFT, padx=5)
        self.data_ext_entry = ctk.CTkEntry(self.data_ext_frame, 
                                           placeholder_text="1",
                                           width=50)
        self.data_ext_entry.insert(0, "1")
        self.data_ext_entry.pack(side=ctk.LEFT, padx=5)
        self.data_ext_value_label = ctk.CTkLabel(self.data_ext_frame, text=self.data_ext_entry.get())
        self.data_ext_value_label.pack(side=ctk.LEFT, padx=5)
        self.data_ext_entry.bind('<Return>', lambda event=None: self.update_data_ext())
        self.data_ext_frame.pack(pady=5)

        # Header extention
        self.hdr_ext_frame = ctk.CTkFrame(self.img_tabview.tab("Fits Config"))
        self.hdr_ext_label = ctk.CTkLabel(self.hdr_ext_frame,
                                            text="FITS Header Extention:")
        self.hdr_ext_label.pack(side=ctk.LEFT, padx=5)
        self.hdr_ext_entry = ctk.CTkEntry(self.hdr_ext_frame, 
                                        placeholder_text="0",
                                        width=50)
        self.hdr_ext_entry.insert(0, "0")
        self.hdr_ext_entry.pack(side=ctk.LEFT, padx=5)
        self.hdr_ext_value_label = ctk.CTkLabel(self.hdr_ext_frame, 
                                                text=self.hdr_ext_entry.get())
        self.hdr_ext_value_label.pack(side=ctk.LEFT, padx=5)
        self.hdr_ext_entry.bind('<Return>', lambda event=None: self.update_hdr_ext())
        self.hdr_ext_frame.pack(pady=5)

        # WCS extention
        self.wcs_ext_frame = ctk.CTkFrame(self.img_tabview.tab("Fits Config"))
        self.wcs_ext_label = ctk.CTkLabel(self.wcs_ext_frame,
                                          text="FITS WCS Extention:")
        self.wcs_ext_label.pack(side=ctk.LEFT, padx=5)
        self.wcs_ext_entry = ctk.CTkEntry(self.wcs_ext_frame, 
                                          placeholder_text="1",
                                          width=50)
        self.wcs_ext_entry.insert(0, "1")
        self.wcs_ext_entry.pack(side=ctk.LEFT, padx=5)
        self.wcs_ext_value_label = ctk.CTkLabel(self.wcs_ext_frame,
                                                text=self.wcs_ext_entry.get())
        self.wcs_ext_value_label.pack(side=ctk.LEFT, padx=5)
        self.wcs_ext_entry.bind('<Return>', lambda event=None: self.update_wcs_ext())
        self.wcs_ext_frame.pack(pady=5)

        # Error extention
        self.err_ext_frame = ctk.CTkFrame(self.img_tabview.tab("Fits Config"))
        self.err_ext_label = ctk.CTkLabel(self.err_ext_frame,
                                          text="FITS Error Extention:")
        self.err_ext_label.pack(side=ctk.LEFT, padx=5)
        self.err_ext_entry = ctk.CTkEntry(self.err_ext_frame, 
                                          placeholder_text="2",
                                          width=50)
        self.err_ext_entry.insert(0, "2")
        self.err_ext_entry.pack(side=ctk.LEFT, padx=5)
        self.err_ext_value_label = ctk.CTkLabel(self.err_ext_frame,
                                                text=self.wcs_ext_entry.get())
        self.err_ext_value_label.pack(side=ctk.LEFT, padx=5)
        self.err_ext_entry.bind('<Return>', lambda event=None: self.update_err_ext())
        self.err_ext_frame.pack(pady=5)

        # Open file button
        self.openfile = ctk.CTkButton(self.img_tabview.tab("Fits Config"), text='Open FITS Image', command=self.open_image)
        self.openfile.pack(pady=20)


        # Image plot lims (vmin, vmax)
        self.vmin_label = ctk.CTkLabel(self.img_tabview.tab("View Options"), text="Vmin:", anchor="w")
        self.vmin_label.pack()
        self.vminslider = ctk.CTkSlider(self.img_tabview.tab("View Options"), from_=0, to=1,  number_of_steps=10, 
                                        command=self.vminslider)
        self.vminslider.set(0.1)
        self.vminslider.pack(pady=0)

        self.vmax_label = ctk.CTkLabel(self.img_tabview.tab("View Options"), text="Vmax:")
        self.vmax_label.pack()
        self.vmaxslider = ctk.CTkSlider(self.img_tabview.tab("View Options"), from_=0, to=1,  number_of_steps=10, 
                                        command=self.vmaxslider)
        self.vmaxslider.set(1)
        self.vmaxslider.pack( pady=0)

        ## Header Keys
        # Open header button
        self.openheader = ctk.CTkButton(self.img_tabview.tab("Header Keys"), text='Open Header', command=self.open_header)
        self.openheader.pack(pady=20)
        # Target key
        self.tar_key_frame = ctk.CTkFrame(self.img_tabview.tab("Header Keys"))
        self.tar_key_label = ctk.CTkLabel(self.tar_key_frame,
                                            text="Target key:")
        self.tar_key_label.pack(side=ctk.LEFT, padx=5)
        self.tar_key_entry = ctk.CTkEntry(self.tar_key_frame, 
                                        placeholder_text="TARGNAME",
                                        width=100)
        self.tar_key_entry.insert(0, "TARGNAME")
        self.tar_key_entry.pack(side=ctk.LEFT, padx=5)
        self.tar_key_value_label = ctk.CTkLabel(self.tar_key_frame, 
                                                text=self.update_tar_key())
        self.tar_key_value_label.pack(side=ctk.LEFT, padx=5)
        self.tar_key_entry.bind('<Return>', lambda event=None: self.update_tar_key())
        self.tar_key_frame.pack(pady=5)
        # Date key
        self.date_key_frame = ctk.CTkFrame(self.img_tabview.tab("Header Keys"))
        self.date_key_label = ctk.CTkLabel(self.date_key_frame,
                                            text="Date key:")
        self.date_key_label.pack(side=ctk.LEFT, padx=5)
        self.date_key_entry = ctk.CTkEntry(self.date_key_frame, 
                                        placeholder_text="DATE-OBS",
                                        width=100)
        self.date_key_entry.insert(0, "DATE-OBS")
        self.date_key_entry.pack(side=ctk.LEFT, padx=5)
        self.date_key_value_label = ctk.CTkLabel(self.date_key_frame, 
                                                text=self.update_date_key())
        self.date_key_value_label.pack(side=ctk.LEFT, padx=5)
        self.date_key_entry.bind('<Return>', lambda event=None: self.update_date_key())
        self.date_key_frame.pack(pady=5)
        # Time key
        self.time_key_frame = ctk.CTkFrame(self.img_tabview.tab("Header Keys"))
        self.time_key_label = ctk.CTkLabel(self.time_key_frame,
                                            text="Time key:")
        self.time_key_label.pack(side=ctk.LEFT, padx=5)
        self.time_key_entry = ctk.CTkEntry(self.time_key_frame, 
                                        placeholder_text="TIME-OBS",
                                        width=100)
        self.time_key_entry.insert(0, "TIME-OBS")
        self.time_key_entry.pack(side=ctk.LEFT, padx=5)
        self.time_key_value_label = ctk.CTkLabel(self.time_key_frame, 
                                                text=self.update_time_key())
        self.time_key_value_label.pack(side=ctk.LEFT, padx=5)
        self.time_key_entry.bind('<Return>', lambda event=None: self.update_time_key())
        self.time_key_frame.pack(pady=5)
        # Exposure key
        self.exptime_key_frame = ctk.CTkFrame(self.img_tabview.tab("Header Keys"))
        self.exptime_key_label = ctk.CTkLabel(self.exptime_key_frame,
                                            text="Exp Time key:")
        self.exptime_key_label.pack(side=ctk.LEFT, padx=5)
        self.exptime_key_entry = ctk.CTkEntry(self.exptime_key_frame, 
                                        placeholder_text="EXPTIME",
                                        width=100)
        self.exptime_key_entry.insert(0, "EXPTIME")
        self.exptime_key_entry.pack(side=ctk.LEFT, padx=5)
        self.exptime_key_value_label = ctk.CTkLabel(self.exptime_key_frame, 
                                                text=self.update_exptime_key())
        self.exptime_key_value_label.pack(side=ctk.LEFT, padx=5)
        self.exptime_key_entry.bind('<Return>', lambda event=None: self.update_exptime_key())
        self.exptime_key_frame.pack(pady=5)
        # PhotoLam key
        self.photolam_key_frame = ctk.CTkFrame(self.img_tabview.tab("Header Keys"))
        self.photolam_key_label = ctk.CTkLabel(self.photolam_key_frame,
                                            text="Photo Conversion key:")
        self.photolam_key_label.pack(side=ctk.LEFT, padx=5)
        self.photolam_key_entry = ctk.CTkEntry(self.photolam_key_frame, 
                                        placeholder_text="PHOTFLAM",
                                        width=100)
        self.photolam_key_entry.insert(0, "PHOTFLAM")
        self.photolam_key_entry.pack(side=ctk.LEFT, padx=5)
        self.photolam_key_value_label = ctk.CTkLabel(self.photolam_key_frame, 
                                                text=self.update_photolam_key())
        self.photolam_key_value_label.pack(side=ctk.LEFT, padx=5)
        self.photolam_key_entry.bind('<Return>', lambda event=None: self.update_photolam_key())
        self.photolam_key_frame.pack(pady=5)
        # Zero point key
        self.zp_key_frame = ctk.CTkFrame(self.img_tabview.tab("Header Keys"))
        self.zp_key_label = ctk.CTkLabel(self.zp_key_frame,
                                            text="Zero-point key:")
        self.zp_key_label.pack(side=ctk.LEFT, padx=5)
        self.zp_key_entry = ctk.CTkEntry(self.zp_key_frame, 
                                        placeholder_text="PHOTZPT",
                                        width=100)
        self.zp_key_entry.insert(0, "PHOTZPT")
        self.zp_key_entry.pack(side=ctk.LEFT, padx=5)
        self.zp_key_value_label = ctk.CTkLabel(self.zp_key_frame, 
                                                text=self.update_zp_key())
        self.zp_key_value_label.pack(side=ctk.LEFT, padx=5)
        self.zp_key_entry.bind('<Return>', lambda event=None: self.update_zp_key())
        self.zp_key_frame.pack(pady=5)

    def update_data_ext(self):
        value = self.data_ext_entry.get()
        self.data_ext_value_label.configure(text=f"{value}")

    def update_hdr_ext(self):
        value = self.hdr_ext_entry.get()
        self.hdr_ext_value_label.configure(text=f"{value}")

    def update_wcs_ext(self):
        value = self.wcs_ext_entry.get()
        self.wcs_ext_value_label.configure(text=f"{value}")

    def update_err_ext(self):
        value = self.err_ext_entry.get()
        self.err_ext_value_label.configure(text=f"{value}")

    def update_tar_key(self):
        if self.fitsimg is not None:
            value = self.tar_key_entry.get()
            if value != "-1":
                try:
                    self.tar_key_value_label.configure(text=f"{self.fitsimg.hdr[value]}")
                except:
                    pass #-> rever

    def update_date_key(self):
        if self.fitsimg is not None:
            value = self.date_key_entry.get()
            if value != "-1":
                try:
                    self.date_key_value_label.configure(text=f"{self.fitsimg.hdr[value]}")
                except:
                    pass #-> rever

    def update_time_key(self):
        if self.fitsimg is not None:
            value = self.time_key_entry.get()
            if value != "-1":
                try:
                    self.time_key_value_label.configure(text=f"{self.fitsimg.hdr[value]}")
                except:
                    pass #-> rever

    def update_exptime_key(self):
        if self.fitsimg is not None:
            value = self.exptime_key_entry.get()
            if value != "-1":
                try:
                    self.exptime_key_value_label.configure(text=f"{self.fitsimg.hdr[value]}")
                except:
                    pass #-> rever

    def update_photolam_key(self):
        if self.fitsimg is not None:
            value = self.photolam_key_entry.get()
            if value != "-1":
                try:
                    self.photolam_key_value_label.configure(text=f"{self.fitsimg.hdr[value]}")
                except:
                    pass #-> rever

    def update_zp_key(self):
        if self.fitsimg is not None:
            value = self.zp_key_entry.get()
            if value != "-1":
                try:
                    self.zp_key_value_label.configure(text=f"{self.fitsimg.hdr[value]}")
                except:
                    pass #-> rever
        # else:
        #     self.zp_key_value_label.configure(text=f"None")
      

    def update_header_keys(self):
        self.update_tar_key()
        self.update_date_key()
        self.update_time_key()
        self.update_exptime_key()
        self.update_photolam_key()
        self.update_zp_key()

    def open_image(self):
        file_path = ctk.filedialog.askopenfilename(filetypes=[("All files","*.*")], initialdir ='/home/mario/sync/projetos/qh/hst/data/')
        if file_path:
            # # try:
                self.fitsimg = loadimage(file_path,
                                         data_ext=int(self.data_ext_entry.get()),
                                         header_ext=int(self.hdr_ext_entry.get()),
                                         wcs_ext=int(self.wcs_ext_entry.get()),
                                         err_ext=int(self.err_ext_entry.get()))
                self.show_image()
                self.update_header_keys()
                self.update_info()

    def update_info(self):
        self.info.image_name = self.fitsimg.label
        self.info.target = self.tar_key_value_label.cget("text")
        self.info.date = self.date_key_value_label.cget("text")
        self.info.time = self.time_key_value_label.cget("text")
        self.info.time = self.exptime_key_value_label.cget("text")
        self.info.photconv = self.photolam_key_value_label.cget("text")
        self.info.zp = self.zp_key_value_label.cget("text")
        self.info.update_text()


    def show_image(self):
            self.viewer.img = self.fitsimg.view(self.viewer.ax,
                                                vmin=self.vmin,
                                                vmax=self.vmax, zorder=0)
            self.viewer.img.set_data(self.fitsimg.data)
            self.viewer.canvas.draw()

    def vminslider(self, slider):
        r"""
        """
        self.vmin = slider
        self.show_image()

    def vmaxslider(self, slider):
        r"""
        """
        self.vmax = slider 
        self.show_image()
    
    def open_header(self):
        if self.fitsimg is not None:
            # Create a new Tkinter window
            header_window = ctk.CTkToplevel(self)
            header_window.title("Header")
            header_window.geometry("600x720")
            # Create a Text widget to display text
            header_text = self.fitsimg.hdr.__repr__()
            header_widget = ctk.CTkTextbox(header_window, wrap="word")
            # Insert vext
            header_widget.insert("0.0", header_text)  
            header_widget.pack(expand=True, fill='both') 

class SceneControlFrame(ctk.CTkFrame):
        
    def __init__(self, master, viewer, info, **kwargs):
        r"""
        """
        # view frame
        self.viewer = viewer
        # info frame
        self.info = info
        # inheriting
        super().__init__(master, **kwargs)
        # scene control tabs
        self.img_tabview = ctk.CTkTabview(self, command=self.alternate_scene)
        self.img_tabview.add("0")
        self.scene_counter = 0
        self.add_scene_buttom = ctk.CTkButton(self, 
                                            text='Add Scene', 
                                            command=self.add_scene)
        
        self.remove_button = ctk.CTkButton(self, text="Remove Scene", command=self.remove_scene)
        # add link scene with image
        self.scenes = {"0":ImageControlFrame(self.img_tabview.tab("0"),
                                             self.viewer, self.info)}
        self.scenes["0"].pack(fill='both')
        self.img_tabview.pack(fill='both', pady=5)
        self.add_scene_buttom.pack(fill='both', pady=5)
        self.remove_button.pack(fill='both', pady=5)

        # self.img_tabview.bind("<<TabChanged>>", self.alternate_scene)

    @property
    def current(self):
        current_tab = self.img_tabview.get()
        return self.scenes[current_tab]

    def add_scene(self):
        if  len(self.scenes) < 6:
            self.scene_counter += 1
            self.img_tabview.add(f"{self.scene_counter}")
            self.scenes[f"{self.scene_counter}"] = ImageControlFrame(self.img_tabview.tab(f"{self.scene_counter}"),
                                                                     self.viewer, self.info)
            self.scenes[f"{self.scene_counter}"].pack(fill='both')


    def remove_scene(self):
        current_tab = self.img_tabview.get()
        if current_tab:
            self.img_tabview.delete(current_tab)
        self.scenes.pop(current_tab)


    def alternate_scene(self):
        if self.current.fitsimg is not None:
            self.current.show_image()
            self.current.update_header_keys()
            self.current.update_info()

            