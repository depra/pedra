import customtkinter as ctk
import matplotlib.pyplot as plt
import numpy as np

from photutils.aperture import CircularAnnulus, CircularAperture, aperture_photometry

from pedra import loadimage



class PhotometryFrame(ctk.CTkFrame):
    
    def __init__(self, master, viewer, scenes, info, plotter, **kwargs):
        self.clickeddot = False
        self.clickeddotsky_inner = False
        self.clickeddotsky_outer = False
        self.applot_x = False
        self.applot_y = False
        self.photconv = 1
        self.zp = 0
        self.target_photo = None
        super().__init__(master, **kwargs)
        self.viewer = viewer
        self.scenes = scenes
        self.info = info
        self.plotter = plotter
        # self.phototab = ctk.CTkTabview(self)
        # self.phototab.add("Aperture")
        # self.phototab.add("Sky Subtraction")
        # self.phototab.pack()

        # X Pixel
        self.x_frame = ctk.CTkFrame(self)
        self.x_label = ctk.CTkLabel(self.x_frame,
                                           text="x:")
        self.x_label.pack(side=ctk.LEFT, padx=5)
        self.x_entry = ctk.CTkEntry(self.x_frame, 
                                           placeholder_text="500",
                                           width=50)
        self.x_entry.insert(0, "500")
        self.x_entry.pack(side=ctk.LEFT, padx=5)
        self.x_value_label = ctk.CTkLabel(self.x_frame, text=self.x_entry.get())
        self.x_value_label.pack(side=ctk.LEFT, padx=5)
        self.x_entry.bind('<Return>', lambda event=None: self.update_xy())
        self.x_frame.pack(pady=5)

        # Y Pixel
        self.y_frame = ctk.CTkFrame(self)
        self.y_label = ctk.CTkLabel(self.y_frame,
                                           text="y:")
        self.y_label.pack(side=ctk.LEFT, padx=5)
        self.y_entry = ctk.CTkEntry(self.y_frame, 
                                           placeholder_text="500",
                                           width=50)
        self.y_entry.insert(0, "500")
        self.y_entry.pack(side=ctk.LEFT, padx=5)
        self.y_value_label = ctk.CTkLabel(self.y_frame, text=self.y_entry.get())
        self.y_value_label.pack(side=ctk.LEFT, padx=5)
        self.y_entry.bind('<Return>', lambda event=None: self.update_xy())
        self.y_frame.pack(pady=5)

        # Aperture
        self.radius_frame = ctk.CTkFrame(self)
        self.radius_label = ctk.CTkLabel(self.radius_frame,
                                           text="Radius:")
        self.radius_label.pack(side=ctk.LEFT, padx=5)
        self.radius_entry = ctk.CTkEntry(self.radius_frame, 
                                           placeholder_text="15",
                                           width=50)
        self.radius_entry.insert(0, "15")
        self.radius_entry.pack(side=ctk.LEFT, padx=5)
        self.radius_value_label = ctk.CTkLabel(self.radius_frame, text=self.radius_entry.get())
        self.radius_value_label.pack(side=ctk.LEFT, padx=5)
        self.radius_entry.bind('<Return>', lambda event=None: self.update_radius())
        self.radius_frame.pack(pady=5)

        ## Sky Subtraction
        self.skysub_checkbox = ctk.CTkCheckBox(self, text="Sky Subtraction", command=self.update_skysub)
        self.skysub_checkbox.select()
        self.skysub_checkbox.pack(pady=5)
        # inner radius
        self.skyradius_inner_frame = ctk.CTkFrame(self)
        self.skyradius_inner_label = ctk.CTkLabel(self.skyradius_inner_frame,
                                           text="Inner Radius:")
        self.skyradius_inner_label.pack(side=ctk.LEFT, padx=5)
        self.skyradius_inner_entry = ctk.CTkEntry(self.skyradius_inner_frame, 
                                           placeholder_text="20",
                                           width=50)
        self.skyradius_inner_entry.insert(0, "20")
        self.skyradius_inner_entry.pack(side=ctk.LEFT, padx=5)
        self.skyradius_inner_label = ctk.CTkLabel(self.skyradius_inner_frame, text=self.skyradius_inner_entry.get())
        self.skyradius_inner_label.pack(side=ctk.LEFT, padx=5)
        self.skyradius_inner_entry.bind('<Return>', lambda event=None: self.update_skyradius_inner())
        self.skyradius_inner_frame.pack(pady=5)
        # outter radius
        self.skyradius_outer_frame = ctk.CTkFrame(self)
        self.skyradius_outer_label = ctk.CTkLabel(self.skyradius_outer_frame,
                                           text="outer Radius:")
        self.skyradius_outer_label.pack(side=ctk.LEFT, padx=5)
        self.skyradius_outer_entry = ctk.CTkEntry(self.skyradius_outer_frame, 
                                           placeholder_text="20",
                                           width=50)
        self.skyradius_outer_entry.insert(0, "25")
        self.skyradius_outer_entry.pack(side=ctk.LEFT, padx=5)
        self.skyradius_outer_label = ctk.CTkLabel(self.skyradius_outer_frame, text=self.skyradius_outer_entry.get())
        self.skyradius_outer_label.pack(side=ctk.LEFT, padx=5)
        self.skyradius_outer_entry.bind('<Return>', lambda event=None: self.update_skyradius_outer())
        self.skyradius_outer_frame.pack(pady=5)
        self.aperture = CircularAperture([500, 500], 
                                         r=float(self.radius_entry.get()))
        self.skyaperture = CircularAnnulus([500, 500], 
                                           r_in=float(self.skyradius_inner_entry.get()),
                                           r_out=float(self.skyradius_outer_entry.get()))

        #  Aperture Extract
        self.photoextract_button = ctk.CTkButton(self,  text='Extract Photometry', 
                                                 command=self.extract_aperture)
        self.photoextract_button.pack()

    def extract_aperture(self):
        r"""
        """
        app = aperture_photometry(self.scenes.current.fitsimg.data, self.aperture, error=self.scenes.current.fitsimg.err)
        if not self.skysub_checkbox.get():
            pass
        
        print(app)
        f = app['aperture_sum'][0] * self.photconv
        f_err = ''
        mag = - 2.5* np.log10(f) + self.zp
        mag_low = ''
        mag_up = ''
        if self.scenes.current.fitsimg.err is not None:
            count_err = app['aperture_sum_err'][0]
            f_err = count_err * self.photconv
            mag_low = (- 2.5* np.log10(f + f_err) + self.zp) - mag
            mag_up = (- 2.5* np.log10(f - f_err) + self.zp)  - mag
        ra = ''
        dec = ''
        # if self.scenes.current.fitsimg.wcs is not None:
        #     ra, dec = self.scenes.current.fitsimg.wcs

        print(f, f_err, mag, mag_low, mag_up)
        self.target_photo = {'x': app['xcenter'][0],
                             'y': app['ycenter'][0],
                             'ra': ra,
                             'dec': dec,
                             'flux': f,
                             'flux_err': f_err,
                             'mag': mag,
                             'mag_low': mag_low,
                             'mag_up': mag_up,
                             'count': app['aperture_sum'][0],
                             'count_err': count_err
                             }
        
        self.update_info()

    def update_info(self):
        r"""
        """
        if self.target_photo is not None:
            self.info.flux = self.target_photo['flux']
            self.info.mag = self.target_photo['mag']
            self.info.count = self.target_photo['count']
            self.info.update_text()

    def update_skysub(self):
        if self.skysub_checkbox.get():
            self.skyradius_inner_entry.configure(state='normal', fg_color='white')
            self.skyradius_inner_label.configure(text=self.skyradius_inner_entry.get())
            self.skyradius_outer_entry.configure(state='normal', fg_color='white')
            self.skyradius_outer_label.configure(text=self.skyradius_outer_entry.get())
        else:
            self.skyradius_inner_entry.configure(state='disabled', fg_color='gray')
            self.skyradius_inner_label.configure(text=self.skyradius_inner_entry.get())
            self.skyradius_outer_entry.configure(state='disabled', fg_color='gray')
            self.skyradius_outer_label.configure(text=self.skyradius_outer_entry.get())
        if self.scenes.current.fitsimg is not None:
            self.remove_aperture()
            self.show_aperture()


    def update_skyradius_outer(self):
        r"""
        """
        value = self.skyradius_outer_entry.get()
        self.skyradius_outer_label.configure(text=f"{value}")
        self.skyaperture.r_out = float(value)
        if self.scenes.current.fitsimg is not None:
            self.remove_aperture()
            self.show_aperture()

    def update_skyradius_inner(self):
        r"""
        """
        value = self.skyradius_inner_entry.get()
        self.skyradius_inner_label.configure(text=f"{value}")
        self.skyaperture.r_in = float(value)
        if self.scenes.current.fitsimg is not None:
            self.remove_aperture()
            self.show_aperture()

    def update_radius(self):
        value = self.radius_entry.get()
        self.radius_value_label.configure(text=f"{value}")
        self.aperture.r = float(value)
        if self.scenes.current.fitsimg is not None:
            self.remove_aperture()
            self.show_aperture()

    def update_xy(self):
        x_value = int(self.x_entry.get())
        y_value = int(self.y_entry.get())
        self.x_value_label.configure(text=f"{x_value}")
        self.y_value_label.configure(text=f"{y_value}")
        self.aperture.positions = [x_value, y_value]
        # if self.scenes.current.fitsimg.wcs:
        #     ra, dec = self.scenes.current.fitsimg.wcs.pixel_to_world_values([[x_value, y_value]])[0]
        #     self.info.clicklabel.configure(text=f"Clicked Coordinates: x={x_value}, y={y_value} ra={np.round(ra, decimals=4)} dec={np.round(dec, decimals=4)}")
        # else:
        #     self.info.clicklabel.configure(text=f"Clicked Coordinates: x={x_value}, y={y_value}")
        if self.scenes.current.fitsimg is not None:
            self.remove_aperture()
            self.show_aperture()

    def show_aperture(self):
        r"""
        """
        x, y = self.aperture.positions
        apcircle = plt.Circle((x, y), self.aperture.r, zorder=1000,  edgecolor='firebrick', facecolor='none')
        self.clickeddot = self.viewer.ax.add_artist(apcircle)
        if self.skysub_checkbox.get():
            skyincircle = plt.Circle((x, y), self.skyaperture.r_in, zorder=1000,  edgecolor='steelblue', facecolor='none')
            self.clickeddotsky_inner = self.viewer.ax.add_artist(skyincircle)
            skyoucircle = plt.Circle((x, y), self.skyaperture.r_out, zorder=1000,  edgecolor='steelblue', facecolor='none')
            self.clickeddotsky_outer = self.viewer.ax.add_artist(skyoucircle)
        self.viewer.canvas.draw()
        self.aperture_plot()
    
    def remove_aperture(self):
        if self.clickeddot:
            self.clickeddot.remove()
            self.clickeddot = False
        if self.clickeddotsky_inner:
            self.clickeddotsky_inner.remove()
            self.clickeddotsky_inner = False
        if self.clickeddotsky_outer:
            self.clickeddotsky_outer.remove()
            self.clickeddotsky_outer = False
        if self.applot_x:
            self.applot_x.remove()
            self.applot_x = False        
            self.applot_y.remove()
            self.applot_y = False 

    def aperture_plot(self):
        r"""
        """
        if not self.skysub_checkbox.get():
            offset = int(self.aperture.r + 10) 
        else:
            offset = int(self.skyaperture.r_out + 10)
        cutoff = self.scenes.current.fitsimg.trim(tlims=[[int(self.aperture.positions[0]-offset),
                                                          int(self.aperture.positions[0]+offset)],
                                                          [int(self.aperture.positions[1]-offset),
                                                           int(self.aperture.positions[1]+offset)]], 
                                                  prefix='t')
        x_axis = range(-offset,+offset)
        x_cutoff = cutoff.data.sum(axis=0)
        y_cutoff = cutoff.data.sum(axis=1)
        self.plotter.ax.clear()


        self.applot_x = self.plotter.ax.plot(x_axis, x_cutoff, c='teal', lw=0.8, label='X Profile')[0]
        self.applot_y = self.plotter.ax.plot(x_axis, y_cutoff, c='green', lw=0.8, label='Y Profile')[0]
        self.plotter.ax.axvline(-self.aperture.r,  c='firebrick', ls='-', lw=1.5, label='Circular Aperture')
        self.plotter.ax.axvline(+self.aperture.r,  c='firebrick', ls='-', lw=1.5)
        self.plotter.ax.axvline(0,  c='k', ls='--', lw=0.5, alpha=0.5)

        if self.skysub_checkbox.get():
            self.plotter.ax.axvline(-self.skyaperture.r_in,  c='steelblue', ls='-', lw=1.5, label='Sky Aperture')
            self.plotter.ax.axvline(+self.skyaperture.r_in,  c='steelblue', ls='-', lw=1.5)
            self.plotter.ax.axvline(-self.skyaperture.r_out,  c='steelblue', ls='-', lw=1.5)
            self.plotter.ax.axvline(+self.skyaperture.r_out,  c='steelblue', ls='-', lw=1.5)
        # self.plotter.ax.autoscale(enable=True, axis='both', tight=None)
        # self.plotter.fig.update()
        self.plotter.ax.legend(frameon=True, ncols=2, fontsize=10, loc='upper left')
        self.plotter.ax.set_xlabel('Pixel')
        self.plotter.ax.set_ylabel('Count')

        self.plotter.canvas.draw()
