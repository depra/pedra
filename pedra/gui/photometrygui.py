import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import customtkinter as ctk
# import tkinter as tk


from core import PEDRAAppBase
from aperturegui import PhotometryFrame
from detectsourcesgui import DetectSourcesFrame, ZeroPointFrame, AsteroidIdentifyFrame

ctk.set_appearance_mode("dark")

class PhotoAnalysisFrame(ctk.CTkFrame):
    r"""
    """
    def __init__(self, master, viewer, scenes, info, plotter, **kwargs):
        super().__init__(master, **kwargs)
        self.viewer = viewer
        self.scenes = scenes
        self.info = info
        self.plotter = plotter
        self.grid_columnconfigure(0, weight=1)
        self.grid_columnconfigure(1, weight=1)
        self.grid_rowconfigure(0, weight=1)
        self.grid_rowconfigure(1, weight=1)

        ### Photometry tab
        self.photo_tab = ctk.CTkTabview(self)
        self.photo_tab.add("Aperture")
        self.photo_tab.add("PSF")
        self.photo_tab.add("Config")
        ## Aperture Frame
        self.aperture_frame = PhotometryFrame(self.photo_tab.tab("Aperture"), 
                                            viewer=self.viewer, scenes=self.scenes,
                                             info=self.info, plotter=self.plotter,
                                            corner_radius=2.5)
        self.aperture_frame.pack(fill='both', expand=True)
        ## Photo Config Frame
        # Photo conv
        self.photconv_frame = ctk.CTkFrame(self.photo_tab.tab("Config"))
        self.photconv_label = ctk.CTkLabel(self.photconv_frame,
                                           text="Photo Conv Factor:")
        self.photconv_label.pack(side=ctk.LEFT, padx=5)
        self.photconv_entry = ctk.CTkEntry(self.photconv_frame, 
                                           placeholder_text="1",
                                           width=80)
        self.photconv_entry.insert(0, "1")
        self.photconv_entry.pack(side=ctk.LEFT, padx=5)
        self.photconv_label = ctk.CTkLabel(self.photconv_frame, text=self.photconv_entry.get())
        self.photconv_label.pack(side=ctk.LEFT, padx=5)
        self.photconv_entry.bind('<Return>', lambda event=None: self.update_photconv())
        self.photconv_frame.pack(pady=5)
        # ZP 
        self.zp_frame = ctk.CTkFrame(self.photo_tab.tab("Config"))
        self.zp_label = ctk.CTkLabel(self.zp_frame,
                                           text="Zero:")
        self.zp_label.pack(side=ctk.LEFT, padx=5)
        self.zp_entry = ctk.CTkEntry(self.zp_frame, 
                                           placeholder_text="0",
                                           width=80)
        self.zp_entry.insert(0, "0")
        self.zp_entry.pack(side=ctk.LEFT, padx=5)
        self.zp_label = ctk.CTkLabel(self.zp_frame, text=self.zp_entry.get())
        self.zp_label.pack(side=ctk.LEFT, padx=5)
        self.zp_entry.bind('<Return>', lambda event=None: self.update_zp())
        self.zp_frame.pack(pady=5)

        ### Detect tab
        self.detect_tab = ctk.CTkTabview(self)
        self.detect_tab.add("Detect Sources")
        self.detect_tab.add("Zero Point")
        self.detect_tab.add("Asteroid ID")
        ## Detect Frame
        self.detect_frame = DetectSourcesFrame(self.detect_tab.tab("Detect Sources"), 
                                               viewer=self.viewer, scenes=self.scenes,
                                               info=self.info,
                                               corner_radius=2.5)
        self.detect_frame.pack(fill='both', expand=True)

        # self.detect_frame = DetectFrame(self, 
        #                                     viewer=self.viewer, scenes=self.scenes,
        #                                     corner_radius=2.5)
        

        # self.detect_frame.grid(row=0, column=2, sticky="nsew",padx=5, pady=5)
        ## Asteroid Identify 
        self.astid_frame = AsteroidIdentifyFrame(self.detect_tab.tab("Asteroid ID"), 
                                                 viewer=self.viewer, scenes=self.scenes,
                                                 info=self.info,
                                                 corner_radius=2.5)
        self.astid_frame.pack(fill='both', expand=True)     

        # Grid
        self.photo_tab.grid(row=0, column=0, sticky="nsew",padx=5, pady=5)
        self.detect_tab.grid(row=0, column=1, sticky="nsew",padx=5, pady=5)

        # Bindings
        self.scenes.current.openfile.bind('<Button-1>', self.update_photoconfig)

    def update_photoconfig(self, event):
        photocv = self.scenes.current.photolam_key_value_label.cget("text")
        self.aperture_frame.photconv = float(photocv)
        self.photconv_label.configure(text=f"{photocv}")
        zp = self.scenes.current.zp_key_value_label.cget("text")
        self.aperture_frame.zp = float(zp)
        self.zp_label.configure(text=f"{zp}")
        
    def update_photconv(self):
        if self.scenes.current.fitsimg is not None:
            value = self.photconv_entry.get()
            self.aperture_frame.photconv = float(value)
            self.photconv_label.configure(text=f"{value}")

    def update_zp(self):
        if self.scenes.current.fitsimg is not None:
            value = self.zp_entry.get()
            self.aperture_frame.zp = float(value)
            self.zp_label.configure(text=f"{value}")

class PEDRAPhotometry(PEDRAAppBase):
    
    def __init__(self):
        self.target_photo = False
        super().__init__()
        
        self.analysisframe = PhotoAnalysisFrame(self, viewer=self.viewer, 
                                                scenes=self.scenes, info=self.info,
                                                plotter=self.plotter)
        self.analysisframe.grid(row=1, column=1, columnspan=2, sticky="nsew",padx=5, pady=5)
        self.viewer.canvas.mpl_connect('button_press_event', self.on_mouse_click)

        # Results
        self.results.results_table = pd.DataFrame(columns=['Image File', 
                                                  'Target', 'RA_ephem', 'Dec_ephem',
                                                  'V_ephem', 'X_ephem', 'Y_ephem',
                                                  'AP_radius', 'AP_X', 'AP_Y',
                                                  'SKY_REM', 'AP_RADIUS_IN', 'AP_RADIUS_OUT',
                                                  'AP_RA', 'AP_DEC', 'AP_COUNT',  
                                                  'AP_FLUX', 'AP_FLUX_ERR', 
                                                  'AP_MAG', 'AP_MAG_ERR'])
        self.results.store_button.configure(command=self.store_results)

    def store_results(self):
        results = {'Image File': self.scenes.current.fitsimg.label,
                   'Target': self.info.target,
                   'RA_ephem': self.info.raast,
                   'Dec_ephem': self.info.decast,
                   'V_ephem':self.info.vast,
                   'X_ephem':self.info.xast,
                   'Y_ephem':self.info.yast,
                   'AP_radius':self.info.radius, 
                   'AP_X': self.info.x, 
                   'AP_Y':self.info.y,
                   'SKY_REM': self.info.decast, 
                   'AP_RADIUS_IN': self.info.decast, 
                   'AP_RADIUS_OUT': self.info.decast,
                   'AP_RA': self.info.ra, 
                   'AP_DEC': self.info.dec, 
                   'AP_COUNT': self.info.count,
                   'AP_FLUX': self.info.flux, 
                   'AP_FLUX_ERR': self.info.decast, 
                   'AP_MAG': self.info.mag, 
                   'AP_MAG_ERR': self.info.decast
                   }
        results_data = pd.DataFrame([results])
        self.results.results_table = pd.concat([self.results.results_table, 
                                                results_data], ignore_index=True)

    def on_mouse_click(self, event):
        # Get coordinates of mouse click
        x = int(event.xdata)
        y = int(event.ydata)
        if self.analysisframe.detect_frame.track_sources.get():
            if self.analysisframe.detect_frame.sources is not None:
                x, y = self.analysisframe.detect_frame.nearest_source([x,y])

        self.analysisframe.aperture_frame.x_value_label.configure(text=f'{x}')
        self.analysisframe.aperture_frame.y_value_label.configure(text=f'{y}')
        self.analysisframe.aperture_frame.aperture.positions = [x, y]
        # Display clicked coordinates
        self.analysisframe.aperture_frame.remove_aperture()
        self.analysisframe.aperture_frame.show_aperture()
        
if __name__ == '__main__':
    app = PEDRAPhotometry()
    app.mainloop()